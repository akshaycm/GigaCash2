Gigacash [GCASH]
=====================================

[![Build Status](https://travis-ci.org/Gigacash-Project/Gigacash.svg?branch=master)](https://travis-ci.org/Gigacash-Project/Gigacash) [![GitHub version](https://badge.fury.io/gh/Gigacash-Project%2FGigacash.svg)](https://badge.fury.io/gh/Gigacash-Project%2FGigacash)

GigaCash is a open source cryptocurrency that focuses on privacy, speed, decentralization and community decisions. GigaCash uses the QUARK algorithm with POS consensus and Masternodes to create a secure network with full nodes across the globe, this ensures the network is decentralized and no one person has control or ownership. Unlike most other currencies, decisions on GigaCash’s future are made by the community. Our long term goal is mass adoption and to be a top 100 cryptocurrency within 2 years, we aim to complete community projects and create a payment gateway which our partners will beable to implement to accept payment with any cryptocurrency for services, products, bills and more, while giving GigaCash holders extra benefits. We are also looking to create a Decentralized Exchange which will give masternodes and stakers 90% of trading fees.

More information at [Gigacash.org](http://www.Gigacash.org)

### Coin Specs
<table>
<tr><td>Ticker Symbol</td><td>GCASH</td></tr>
<tr><td>Algorithm</td><td>Quark</td></tr>
<tr><td>Type</td><td>PoW - PoS Hybrid*</td></tr>
<tr><td>Block Time</td><td>60 Seconds</td></tr>
<tr><td>Difficulty Retargeting</td><td>Every Block</td></tr>
<tr><td>Min Stake Age</td><td>1 Hour</td></tr>
<tr><td>Maz Stake Age</td><td>-</td></tr>
<tr><td>Premine/Initial Supply</td><td>4 Million</td></tr>
</table>



### PoS/PoW Block Details
<table>
<tr><td>Proof Of Work Phase</td><td>1-2000</td></tr>
<tr><td>Proof of Stake Phase</td><td>2001+</td></tr>
</table>

### Masternodes/Staking
<table>
<tr><td style="
    background: #3f0d26;
    color: white;
    text-align: center;
">Masternode Collateral</td>
<td style="
    background: #3f0d26;
    color: white;
    text-align: center;
">10,000 GCASH</td></tr>
<tr><td style="
    background: #3f0d26;
    color: white;
    text-align: center;
">Masternodes</td><td>70% of each block</td></tr>
<tr><td style="
    background: #3f0d26;
    color: white;
    text-align: center;
">Staking</td><td>30% of each block</td></tr>
</table>

### Rewards
<table>
<tr><td >BLOCK</td><td >REWARD</td>
<td >MASTERNODE 70%</td>
<td>POS 30%</td></tr>

<tr><td >1 – 20,000</td><td >20</td><td >14</td><td>6</td></tr>
<tr><td >20,001 – 60,000</td><td >150</td><td >105</td><td>45</td></tr>
<tr><td>60,001 – 100,000</td><td>125</td><td>87.5</td><td>37.5</td></tr>
<tr><td>100,001 – 140,000</td><td>100</td><td>70</td><td>30</td></tr>
<tr><td>140,001 – 180,000</td><td>80</td><td>56</td><td>24</td></tr>
<tr><td>180,001 – 220,000</td><td>60</td><td>42</td><td>18</td></tr>
<tr><td>220,001 – 270,000</td><td>50</td><td>35</td><td>15</td></tr>
<tr><td>270,001 – 320,000</td><td>40</td><td>28</td><td>12</td></tr>
<tr><td>320,001 – 370,000</td><td>30</td><td>21</td><td>9</td></tr>
<tr><td>370,001 – 420,000</td><td>20</td><td>14</td><td>6</td></tr>
<tr><td>420,001 – 470,000</td><td>15</td><td>10.5</td><td>4.5</td></tr>
<tr><td>470,001 - 550,000</td><td>10</td><td>7</td><td>3</td></tr>
<tr><td>550,001 - 1,000,000</td><td>8</td><td>5.6</td><td>2.4</td></tr>
<tr><td>1,000,001 - 2,500,000</td><td>5</td><td>3.5</td><td>1.5</td></tr>
<tr><td >2,500,001 +</td><td>3</td><td>2.1</td><td>0.9</td></tr>            
</table>

More details can be found in our [Whitepaper](https://gigacash.org/gigacash-whitepaper.pdf)
