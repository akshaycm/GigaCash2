Sample configuration files for:

SystemD: Gigacashd.service
Upstart: Gigacashd.conf
OpenRC:  Gigacashd.openrc
         Gigacashd.openrcconf
CentOS:  Gigacashd.init

have been made available to assist packagers in creating node packages here.

See doc/init.md for more information.
